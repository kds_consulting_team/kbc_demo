import pandas as pd

# Default Table Output Destination
DEFAULT_TABLE_SOURCE = "/data/in/tables/"
DEFAULT_TABLE_DESTINATION = "/data/out/tables/"

json_obj = {
    'id': '123',
    'col_1': '1',
    'col_2': '2'
}

frame = pd.DataFrame([json_obj])
print(frame)

frame.to_csv(DEFAULT_TABLE_DESTINATION+"frame.csv", index=False)